/*
 * Program - 3
 * WAP to count the digits of the given number.
 */

class Question3{
	public static void main(String[] rutu){
		int num = 942111423;
		int count = 0;
		while(num != 0 ){
			count++;
			num = num/10;
		}
		System.out.println(count);
	}
}

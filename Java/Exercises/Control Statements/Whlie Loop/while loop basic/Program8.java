/*
 * Program - 8
 * WAP to print the countdown of days to submit the assignment
 */

class Question8{
	public static void main(String[] rutu){
		int day = 7;
		while(day > 1){
			System.out.println(day +" days remaining");				
			day--;
		}		
		if(day == 1){
			System.out.println(day + " day remaining");			
			day--;
		}		
		if(day == 0){
			System.out.println(day + " days assignment is overdue");
		}
	}
}


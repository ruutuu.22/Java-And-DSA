/*
 * Program - 4
 * WAP to count the odd digits of the given number.
 */

class Question4{
	public static void main(String[] rutu){
		int num = 987456321;
		int temp = num;
		int count = 0;
		while(num != 0){
			if((num%10)%2 == 0){
				count++;
			}
			num = num/10;
		}
		System.out.println("Count of odd digits in " + temp + " is " + count);
	}
}

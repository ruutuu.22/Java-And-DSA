/*
 * Program - 9
 * WAP to print number in reverse
 */

class Question9{
	public static void main(String[] rutu){
		int num = 987456321;
		int temp = num;
		int rev = 0;
		while(num != 0){
			int rem = num%10;
			rev = (rev*10) + rem;
			num = num/10;
		}
		System.out.println("Reverse of "+ temp + " is " + rev);
	}
}

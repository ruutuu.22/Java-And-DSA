/*
 * Program - 6
 * WAP to print the sum of all even numbers and multiplication of odd numbers between 1 to 10
 */

class Question6{
	public static void main(String[] rutu){
		int sum = 0;
		int product = 1;
		int i = 1;
		while(i <= 10){
			if(i%2 == 0){
				sum = sum + i;
			}else{
				product = product * i;
			}
			i++;
		}
		System.out.println("Sum of even numbers between 1 to 10 = " + sum);
		System.out.println("Multiplication of odd numbers between 1 to 10 = " + product);
	}
}


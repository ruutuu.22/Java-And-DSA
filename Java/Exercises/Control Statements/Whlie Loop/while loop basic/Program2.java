/*
 * Program - 2
 * WAP to calculate the factorial of the given number.
 */

class Question2{
	public static void main(String[] rutu){
		int num = 6;
		int temp = num;
		int fact = 1;
		if(num >= 0){
			while(num != 0){
				fact = fact * num;
				num --;
			}
			System.out.println("Factorial of "+temp+" is "+fact);
		}else{
			System.out.println("Invalid Input.");
		}
	}
}


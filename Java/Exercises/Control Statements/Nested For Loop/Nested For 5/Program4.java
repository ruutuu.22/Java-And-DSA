/*
 * WAP to print all even numbers in reverse order and odd numbers in the standard way.
 * Both Separately.
 * Within a range.
 * Take the start and end from user.
 */

import java.io.*;
class Question4{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter start : ");
		int start = Integer.parseInt(br.readLine());
		System.out.println("Enter end : ");
		int end = Integer.parseInt(br.readLine());

		int num = end;
		for(int i=start; i<=end; i++){
			if(i%2 != 0){
				System.out.print(i + " ");
			}
		}
		System.out.println();
		for(int i=end; i>=start; i--){
			if(i%2 == 0){
				System.out.print(i + " ");
			}
		}
		System.out.println();
	}
}


/*
 * D4 C3 B2 A1
 * A1 B2 C3 D4
 * D4 C3 B2 A1
 * A1 B2 C3 D4
 */

import java.io.*;
class Question1{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter rows");
		int rows = Integer.parseInt(br.readLine());
		int num = rows;
		int ch = 64 + rows;
		for(int i=1; i<=rows; i++){
			for(int j=1; j<=rows; j++){
				if(i%2 == 0){
					System.out.print((char)ch +""+ (num) + " ");
					ch++;
					num++;
				}else{	
					System.out.print((char)ch +""+ (num) + " ");
					ch--;
					num--;
				}
			}
			if(i%2 == 0){
				ch--;
				num --;
			}else{
				ch++;
				num++;
			}
			System.out.println();
		}
	}
}

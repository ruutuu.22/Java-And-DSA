/*
 * Program - 9
 * WAP to merge two given arrays
 */

import java.io.*;
class Question9{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size for arr1 : ");
		int size1 = Integer.parseInt(br.readLine());
		int arr1[] = new int[size1];
		System.out.println("Enter array elements for arr1 : ");
		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array size for arr2 : ");
		int size2 = Integer.parseInt(br.readLine());

		int arr2[] = new int[size2];
		System.out.println("Enter array elements for arr2 : ");
		for(int i=0; i<arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.print("Elements of arr1 are : ");
		for(int i=0; i<arr1.length; i++){
			System.out.print(arr1[i] + " ");
		}
		System.out.println();

		System.out.print("Elements of arr2 are : ");
		for(int i=0; i<arr2.length; i++){
			System.out.print(arr2[i] + " ");
		}
		System.out.println();

		int size3 = size1 + size2;
		int arr3[] = new int[size3];

		System.out.print("Elements after merging two arrays are : ");
		for(int i=0; i<arr3.length; i++){
			if(i < arr1.length){
				arr3[i] = arr1[i];
			}else{
				arr3[i] = arr2[i - arr1.length];
			}
			System.out.print(arr3[i] + " ");
		}
		System.out.println();
	}
}

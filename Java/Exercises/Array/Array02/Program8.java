/*
 * Program - 8
 * WAP to find the uncommon elements between two arrays
 */

import java.io.*;
class Question8{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size for arr1 : ");
		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];

		System.out.println("Enter array elements for arr1 : ");
		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array size for arr2 : ");
		int size2 = Integer.parseInt(br.readLine());

		int arr2[] = new int[size2];
		System.out.println("Enter array elements for arr2 : ");
		for(int i=0; i<arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.print("Uncommon elements between two arrays are : ");
		for(int i=0; i<arr1.length; i++){
			int count = 0;
			for(int j=0; j<arr2.length; j++){
				if(arr1[i] != arr2[j]){
					count++;
				}
			}
			if(count == arr2.length){
				System.out.print(arr1[i] + " ");
			}
		}

		for(int i=0; i<arr2.length; i++){
			int count = 0;
			for(int j=0; j<arr1.length; j++){
				if(arr2[i] != arr1[j]){
					count++;
				}
			}
			if(count == arr1.length){
				System.out.print(arr2[i] + " ");
			}
		}
		System.out.println();
	}
}

/*
 * Program - 5
 * WAP to take size of array from user and also take integer elements from user
 * Find the minimum element from the array
 */

import java.io.*;
class Question5{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		int min = arr[0];
		for(int i=0; i<arr.length; i++){
			if(min > arr[i]){
				min = arr[i];
			}
		}
		System.out.println("Minimum element of array is " + min);
	}
}

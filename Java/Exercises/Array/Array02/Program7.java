/*
 * Program - 7
 * WAP to find the common elements between two arrays
 */

import java.io.*;
class Question7{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr1[] = new int[size];
		int arr2[] = new int[size];

		System.out.println("Enter array elements for arr1 : ");
		for(int i=0; i<arr1.length; i++){
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter array elements for arr2 : ");
		for(int i=0; i<arr2.length; i++){
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.print("Common elements between two arrays are : ");
		for(int i=0; i<arr1.length; i++){
			for(int j=0; j<arr2.length; j++){
				if(arr1[i] == arr2[j]){
					System.out.print(arr1[i] + " ");
				}
			}
		}
		System.out.println();
	}
}

/*
 * Program - 2
 * WAP to find the number of even and odd integers in a given array of integers.
 */

import java.io.*;
class Question2{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		int even_cnt = 0;
		int odd_cnt = 0;

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 == 0){
				even_cnt++;
			}else{
				odd_cnt++;
			}
		}
		System.out.println("Count of even array elements is " + even_cnt);
		System.out.println("Count of odd array elements is " + odd_cnt);
	}
}

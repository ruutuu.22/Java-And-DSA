/*
 * Program - 4
 * WAP to search a specific element from an array and return its index.
 */

import java.io.*;
class Question4{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter an element to search if it is present : ");
		int element = Integer.parseInt(br.readLine());
		int flag = 0;
		for(int i=0; i<arr.length; i++){
			if(element == arr[i]){
				System.out.println(element + " present at index " + i);
				flag = 1;
				break;
			}
		}
		if(flag == 0){
			System.out.println(element + " is not present in array");
		}
	}
}

/*
 * Program - 1
 * WAP to create an array of 'n' integer elements
 * Where 'n' value should be taken from the user
 * Insert the values from the user and find the sum of all elements in the array
 */

import java.io.*;
class Question1{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int sum = 0;
		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			sum = sum + arr[i];
		}
		System.out.println("Sum of array elements is " + sum);
	}
}

/*
 * Program - 1
 * WAP to take size of array from user and also take integer elements from user and
 * print the sum of the odd elements only
 */

import java.io.*;
class Question1{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int sum = 0;
		
		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2 != 0){
				sum = sum + arr[i];
			}
		}
		System.out.println("Sum of odd elements in array is " + sum);
	}
}



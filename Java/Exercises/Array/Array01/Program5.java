/*
 * Program - 5
 * WAP,  take 10 input from the user and print only elements that are divisible by 5
 */

import java.io.*;
class Question5{
	public static void main(String[] rutu) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.print("Numbers divisible by 5 are : ");
		for(int i=0; i<arr.length; i++){
			if(arr[i]%5 == 0){
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}
}


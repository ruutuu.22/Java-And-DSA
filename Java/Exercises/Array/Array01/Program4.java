/*
 * Program - 4
 * WAP take 7 characters as a input, print only vowels from the array
 */

import java.io.*;
class Question4{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size : ");
		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = (char)br.read();
			br.skip(1);
		}


		System.out.print("Vowels in array are : ");
		for(int i=0; i<arr.length; i++){
			if(arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u' || arr[i] == 'A' || arr[i] == 'E' || arr[i] == 'I' || arr[i] == 'O' || arr[i] == 'U'){
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}
}


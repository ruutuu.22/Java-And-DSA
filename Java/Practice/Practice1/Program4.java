/*
 * Write a program to find maximum and minimum numbers from an array
 * note: take array from user.
 * Input:
 * N = 6
 * Arr[] = {3,,34, 1, 156,200, 100}
 * Output:
 * min = 1, max = 200
 */

import java.io.*;
class Question4{
	int minEle(int arr[]){
		int min = arr[0];
		for(int i=1; i<arr.length; i++){
			if(arr[i] < min){
				min = arr[i];
			}
		}
		return min;
	}

	int maxEle(int arr[]){
		int max = arr[0];
		for(int i=1; i<arr.length; i++){
			if(arr[i] > max){
				max = arr[i];
			}
		}
		return max;
	}
	public static void main(String[] args) throws IOException{
		Question4 obj = new Question4();

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the size of array : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array elements are : ");
		for(int x : arr){
			System.out.println(x + " ");
		}

		int min = obj.minEle(arr);
		int max = obj.maxEle(arr);

		System.out.println("Min = " + min);
		System.out.println("Max = " + max);
	}
}

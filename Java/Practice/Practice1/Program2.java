/*
 * Write a Program to Print following Pattern.
 * note: take rows from user.
 * E a D b
 * c C d
 * B e
 * f
 */

import java.io.*;
class Question2{
	public static void main(String[] rutu) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Entre the number of rows : ");
		int rows = Integer.parseInt(br.readLine());

		int ch1 = 65 + rows;
		int ch2 = 97;

		for(int i=1; i<=rows; i++){
			for(int sp = 1; sp<=i-1; sp++){
				System.out.print("  ");
			}
			for(int j=1; j<=rows-i+1; j++){
				if(i%2 == 0){
					if(j%2==0){
						System.out.print((char)ch1 + " ");
						ch1--;
					}else{
						System.out.print((char)ch2 + " ");
						ch2++;
					}
				}else{
					if(j%2==0){
						System.out.print((char)ch2 + " ");
						ch2++;
					}else{
						System.out.print((char)ch1 + " ");
						ch1--;
					}
				}
			}
			System.out.println();
		}
	}
}

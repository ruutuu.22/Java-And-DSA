/*
 * Realtime example
 */

class StockMarket{
	String cName = "Tata Steel";
	float stkPrice = 107.40f;

	void ShareholdingPattern(){
		System.out.println("Promoters");
		System.out.println("Retail");
		System.out.println("Mutual Funds");
	}

	public static void main(String[] args){
		StockMarket obj = new StockMarket();

		System.out.println(obj.cName);
		System.out.println(obj.stkPrice);
		obj.ShareholdingPattern();
	}
}

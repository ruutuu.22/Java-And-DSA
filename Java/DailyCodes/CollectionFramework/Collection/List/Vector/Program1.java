/*
 * Example - 1
 *
 * Vector is Synchronized
 */

import java.util.*;
class Example1{
	public static void main(String[] args){
		Vector v = new Vector();
		v.addElement(10);
		v.addElement(20);
		v.addElement(30);
		v.addElement(40);

		System.out.println(v);

		System.out.println(v.capacity());
		v.addElement(50);
		v.addElement(60);
		v.addElement(70);
		v.addElement(80);
		v.addElement(90);
		v.addElement(100);
		v.addElement(110);

		System.out.println(v.capacity());
		System.out.println(v);

		System.out.println(v.size());

		System.out.println(v.contains(50));

		System.out.println(v.firstElement());
		System.out.println(v.lastElement());

		System.out.println(v.indexOf(50));
		System.out.println(v.elementAt(5));

		v.insertElementAt(35, 3);
		System.out.println(v);

		v.removeElement(30);
		System.out.println(v);

		System.out.println(v.remove(10));
		System.out.println(v);

		System.out.println(v.get(2));
		System.out.println(v.set(2, 30));
		System.out.println(v);

		v.removeAllElements();
		System.out.println(v);

	}
}


/*
 * Example - 2
 */

class Example2_Outer{
	Object m1(){
		System.out.println("In m1 - Outer");
		class Example2_Inner{
			void m1(){
				System.out.println("In m2 - Inner");
			}
		}
		return new Example2_Inner();
	}
}
class Example2_Client{
	public static void main(String[] args){
		Example2_Outer obj = new Example2_Outer();
		obj.m1().m1();
	}
}


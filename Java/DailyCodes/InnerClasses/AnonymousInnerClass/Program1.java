/*
 * Example - 1
 * 
 * Anonymous inner class is a one time use class i.e class can be used once when the obj is created,
 * when new obj is created new anonymous class is created.
 * Anonymous class is a child class.
 * Parent class is the one whoes object is created.
 * Overiding takes places between this 2 classes. 
 */

class Example1_Demo{
	void m1(){
		System.out.println("AAAAAA");
	}
}
class Example1_Client{
	public static void main(String[] args){
		Example1_Demo obj = new Example1_Demo(){
			void m1(){
				System.out.println("BBBBBB");
			}
		};
		obj.m1();
	}
}

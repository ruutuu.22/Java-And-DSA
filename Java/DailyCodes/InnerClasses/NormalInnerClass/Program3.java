/*
 * Example - 3
 *
 * From Inner class we can access STATIC as well as NON - STATIC variables or functions of the Outer class.
 */

class Example3_Outer{
	int x = 10;
	static int y = 20;
	class Example3_Inner{
		void m1(){
			System.out.println(x);
			System.out.println(y);
			m2();
		}
	}

	void m2(){
		System.out.println("In m2 - OUTER");
	}
}

class Example3_Client{
	public static void main(String[] args){
		Example3_Outer obj = new Example3_Outer();
		Example3_Outer.Example3_Inner obj1 = obj.new Example3_Inner();
		obj1.m1();
	}
}

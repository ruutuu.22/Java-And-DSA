/*
 * Example - 1
 */

class Example1_Outer{
	class Example1_Inner{
		void m1(){
			System.out.println("In m1 - INNER");
		}
	}

	void m2(){
		System.out.println("In m2 - OUTER");
	}
}

class Example1_Client{
	public static void main(String[] args){
		Example1_Outer obj = new Example1_Outer();
		obj.m2();

		Example1_Outer.Example1_Inner obj1 = obj.new Example1_Inner();
		obj1.m1();
	}
}

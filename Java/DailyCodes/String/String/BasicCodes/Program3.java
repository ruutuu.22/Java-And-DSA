/*
 * Example - 3
 */

class Example3{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = "Sadvelkar";

		System.out.println(str1+str2);
		
		String str3 = "RutuparnSadvelkar";
		String str4 = str1 + str2;		//goes with new keyword

		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}

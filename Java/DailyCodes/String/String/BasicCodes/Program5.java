/*
 * Example - 5
 */

class Example5{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = "Sadvelkar";

		String str3 = str1 + str2;
		String str4 = str1.concat(str2);

		System.out.println(str1);
		System.out.println(str2);
	}
}

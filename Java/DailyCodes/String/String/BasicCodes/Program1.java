/*
 * Example - 1
 */

class Example1{
	void fun(String str1, String str2){
		System.out.println("fun : str1 => " + System.identityHashCode(str1));
		System.out.println("fun : str2 => " + System.identityHashCode(str2));
		
		String str3 = "Core2Web";
		String str4 = new String("Core2Web");

		System.out.println("fun : str3 => " + System.identityHashCode(str3));
		System.out.println("fun : str4 => " + System.identityHashCode(str4));
	}

	public static void main(String[] args){
		String str1 = "Core2Web";
		String str2 = new String("Core2web");
		char str3[] = {'C', '2', 'W'};

		System.out.println("main : str1 => " + str1);
		System.out.println("main : str2 => " + str2);
		System.out.println("main : str3 => " + str3);		//prints the address due to concatenation(internally calls toString(of Object class))
		System.out.println(str3);				//prints the string

		Example1 obj = new Example1();
		obj.fun(str1, str2);
	}
}


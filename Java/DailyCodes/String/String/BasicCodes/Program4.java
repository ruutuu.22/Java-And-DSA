/*
 * Example - 4
 */
class Example4{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = "Sadvelkar";

		System.out.println(str1);
		System.out.println(str2);

		str1.concat(str2);

		System.out.println(str1);
		System.out.println(str2);
	}
}


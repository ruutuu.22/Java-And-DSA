/*
 * Example - 6
 * HashCode()
 */

class Example6{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = new String("Rutuparn");

		String str3 = "Rutuparn";
		String str4 = new String("Rutuparn");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
		System.out.println(str4.hashCode());
	}
}


/*
 * Method - 5
 *
 * Method => public int compareToIgnoreCase(String str2);
 *
 * Description => It compares the str1 and str2 (case insensitive).
 *
 * Parameters => String
 *
 * Return type => Integer
 */

class Method5{
	public static void main(String[] args){
		String str1 = "Rutu";
		String str2 = "rutuparn";
		System.out.println(str1.compareToIgnoreCase(str2));
	}
}

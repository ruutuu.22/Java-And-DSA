/*
 * Method - 1
 *
 * Method  => public String concat(String str);
 *
 * Description => concatenate string to this string i.e another string is concatenated to first string.
 *
 * Parameters => String
 *
 * Return Type => String
 */

class Method1{
	public static void main(String[] args){
		String str1 = "Core2";
		String str2 = "Web";
		String str3 = str1.concat(str2);
		System.out.println(str3);
	}
}

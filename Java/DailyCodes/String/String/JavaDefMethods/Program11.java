/*
 * Method - 11
 *
 * Method => public int lastIndexOf(int ch, int fromIndex);
 *
 * Description => Finds the last instance of the character in the given String.
 *
 * Parameters => character(ch to find), integer(index to start the search)
 *
 *Return type => Integer
 */

class Method11{
	public static void main(String[] args){
		String str = "Rutuparn";
		System.out.println(str.lastIndexOf('u', 0));
		System.out.println(str.lastIndexOf('u', 1));
		System.out.println(str.lastIndexOf('u', 5));
	}
}


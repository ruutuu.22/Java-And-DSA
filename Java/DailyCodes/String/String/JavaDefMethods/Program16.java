/*
 * Method - 16
 *
 * Method => public String toUpperCase();
 *
 * Description => It uppercases to the string.
 *
 * Example => str.toUpperCase();
 *
 * Parameter => no arguements.
 *
 *Return type => String
 */

class Method16{
	public static void main(String[] args){
		String str = "Hello World!";
		System.out.println(str.toUpperCase());
	}
}


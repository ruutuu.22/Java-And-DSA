/*
 * Method - 6
 *
 * Method => public boolean equals(Object anObject);
 *
 * Description => Predicate which compares and object to this. This is true only for string with the same character sequence.
 *
 * Parameters => Object(anObject)
 *
 * Return type => Boolean
 */

class Method6{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = new String("Rutuparn");
		System.out.println(str1.equals(str2));
	}
}

/*
 * Method - 10
 *
 * Method => public int indexOf(int ch, int fromIndex);
 *
 * Description => Finds the first instance of the character in the given String.
 *
 * Parameters => character(ch to find), integer(index to start the search)
 *
 *Return type => Integer
 */

class Method10{
	public static void main(String[] args){
		String str = "Rutuparn";
		System.out.println(str.indexOf('u', 0));
		System.out.println(str.indexOf('u', 1));
		System.out.println(str.indexOf('u', 2));
	}
}


/*
 * Method - 17
 *
 * Method => public String trim();
 *
 * Description => Trims all the white spaces before and after ofthe string.
 *
 * Example => str.trim();
 *
 * Parameter => no arguements.
 *
 *Return type => String
 */

class Method17{
	public static void main(String[] args){
		String str = "    Hello World !     ";
		System.out.println(str);
		System.out.println(str.trim());
	}
}


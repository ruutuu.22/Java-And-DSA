/*
 * Method - 14
 *
 * Method => public String substring(int i, int j);
 *
 * Description => creates a substring of the given String starting at a specified index and ending at one character before the specified index.
 *
 * Parameters => integer(starting index), integer(ending index)
 *
 *Return type => String
 */

class Method14{
	public static void main(String[] args){
		String str = "Hello World!";
		System.out.println(str.substring(0,3));
	}
}


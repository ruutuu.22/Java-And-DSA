/*
 * Method - 8
 *
 * Method => public boolean startsWith(String prefix, int toffset);
 *
 * Description => 
 * 1. Predicate which determines if the given String contains the given prefix beginning comparison at toffset.
 * 2. The result is false if the toffset is negative or greater than str.length().
 *
 * Parameters => Prefix String to compare, toffset offset for this string where the comparison Starts.
 *
 * Return type => Boolean
 */

class Method8{
	public static void main(String[] args){
		String str = "Rutuparn";
		System.out.println(str.startsWith("upa", 3));
	}
}


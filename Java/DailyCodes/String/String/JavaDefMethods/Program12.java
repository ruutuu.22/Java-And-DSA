/*
 * Method - 12
 *
 * Method => public String replace(char oldChar, char newChar);
 *
 * Description => replaces every instance of a character in the given String with a new character
 *
 * Parameters => character(old character), character(new character)
 *
 *Return type => String
 */

class Method12{
	public static void main(String[] args){
		String str = "Hello World!";
		String result = str.replace('e', 'a');
		System.out.println(result);
	}
}


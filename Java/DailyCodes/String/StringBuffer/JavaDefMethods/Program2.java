/*
 * Method - 2
 *
 * Method => public synchronized StringBuffer insert(int offset, String str);
 *
 * Description => 
 * 	-Insert the String arguement to the StringBuffer
 * 	-If str is null, the string "null" is used instead.
 *
 * Parameters => Integer(offset the place to insert in this buffer), String(string to insert).
 *
 * Return Type => StringBuffer(this StringBuffer).
 */

class Method2{
	public static void main(String[] args){
		StringBuffer str1 = new StringBuffer("RutuparnSadvelkar");
		
		System.out.println(str1);

		str1.insert(8, "Rajesh");

		System.out.println(str1);
	}
}

/*
 * Method - 3
 *
 * Method => public synchronized StringBuffer delete(int start, int end);
 *
 * Description => 
 * 	-Delete characters from the StringBuffer i.e., delete(10, 12) will delete 10 and 11 but not 12.
 * 	-It is harmless for the end to be larger than length().
 *
 * Parameters => Integer(start the first character to delete), integer(end the index after the last character to delete).
 *
 * Return Type => StringBuffer(this StringBuffer).
 */

class Method3{
	public static void main(String[] args){
		StringBuffer str1 = new StringBuffer("Rutuparn");
		str1.delete(2, 7);
		System.out.println(str1);
	}
}

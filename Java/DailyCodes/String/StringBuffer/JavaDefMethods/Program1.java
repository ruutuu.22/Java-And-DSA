/*
 * Method - 1
 *
 * Method => public synchronized StringBuffer append(String str);
 *
 * Description => 
 * 	-Append the String to the StringBuffer
 * 	-If str is null, the string "null" is appended.
 *
 * Parameters => String(sting to append).
 *
 * Return Type => StringBuffer(this StringBuffer).
 */

class Method1{
	public static void main(String[] args){
		StringBuffer str1 = new StringBuffer("Hello");
		String str2 = "World";
		str1.append(str2);
		System.out.println(str1);
	}
}

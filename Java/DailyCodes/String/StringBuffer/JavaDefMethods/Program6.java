/*
 * Method - 6
 *
 * Method => public synchronized int length();
 *
 * Description => 
 * 	-Get the length of the String this code StringBuffer would create.
 * 	-Not to be confused with the capacity of the StringBuffer.
 *
 * Parameters => No parameters
 *
 * Return Type => Integer(the length of this StringBuffer).
 */

class Method6{
	public static void main(String[] args){
		StringBuffer str = new StringBuffer("Rutuparn");

		System.out.println(str.length());
	}
}

/*
 * Example - 7
 */

class Example7{
	public static void main(String[] args){
		String str1 = "Rutuparn";
		String str2 = new String("Sadvelkar");

		StringBuffer str3 = new StringBuffer("Core2Web");
		str1.concat(str2);
		str3.append(str2);

		System.out.println(str1);
		System.out.println(str2);
		System.out.println(str3);
	}
}

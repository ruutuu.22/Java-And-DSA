/*
 * Example - 4
 */

class Example4{
	public static void main(String[] args){
		StringBuffer str = new StringBuffer();
		System.out.println(str.capacity());
		System.out.println(str);

		str.append("Rutuparn");
		System.out.println(str.capacity());
		System.out.println(str);

		str.append("Rajesh");
		System.out.println(str.capacity());
		System.out.println(str);

		str.append("Sadvelkar");
		System.out.println(str.capacity());
		System.out.println(str);
	}
}

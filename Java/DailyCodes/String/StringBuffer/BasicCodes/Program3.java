/*
 * Example - 3
 *
 * String is immutable, whereas StringBuffer is mutable.
 * Default capacity of StringBuffer is 16 characters.
 * If the string goes out of the default capacity i.e. 16 then it recalculates it's capacity by
 * {[currentCapacity + 1] * 2}
 */

class Example3{
	public static void main(String[] args){
		StringBuffer str1 = new StringBuffer("Rutuparn");
		System.out.println(System.identityHashCode(str1));
		str1.append("Sadvelkar");
		System.out.println(str1);
		System.out.println(System.identityHashCode(str1));
	}
}

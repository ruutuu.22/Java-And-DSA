/*
 * Factorial of particular number
 */

class Question3{
	public static void main(String[] rutu){
		int fact = 1;
		int num = 6;
		for(int i=1; i<=num; i++){
			fact = fact * i;
		}
		System.out.println("Factorial of " + num + " is " + fact);
	}
}

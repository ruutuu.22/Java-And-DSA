/*
 * Number is prime or not
 */

class Question6{
	public static void main(String[] rutu){
		int num = 23;
		int count = 0;
		for(int i=1; i<=num; i++){
			if(num%i == 0){
				count++;
			}
		}
		if(count == 2){
			System.out.println("Number is Prime");
		}else if(count == 1){
			System.out.println("Number is not prime nor composite");
		}else if(count == 0){
			System.out.println("0 cannot be compared for being prime or not");
		}else{
			System.out.println("Number is not prime");
		}
	}
}


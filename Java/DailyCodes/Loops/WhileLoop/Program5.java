/*
 * odd number from 0 to N
 */

class Question3{
	public static void main(String[] rutu){
		int i = 1;
		int N = 10;

		while(i<=N){
			if(i%2 == 1){
				System.out.println(i);
			}
			i++;
		}
	}
}

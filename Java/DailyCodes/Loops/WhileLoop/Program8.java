/*
 * Printing number digit by digit
 */

class Question5{
	public static void main(String[] rutu){
		int num = 6543;
		while(num != 0){
			System.out.println(num%10);
			num = num/10;
		}
	}
}

/*
 * Print perfect squares in particular range
 */

class Question8{
	public static void main(String[] rutu){
		int i = 1;
		int N = 30;

		while(i*i <= N){
			System.out.println(i*i);
			i++;
		}
	}
}


/*
 * Example - 10
 * A 1 B 2
 * C 3 D 4 
 * E 5 F 6
 * G 7 H 8
 */

class Example10{
	public static void main(String[] args){
		int row = 4;
		int num = 1;
		int ch = 65;
		for(int i=1; i<=row; i++){
			for(int j=1; j<=row; j++){
				if(j%2 == 0){
					System.out.print(num + " ");
					num++;
				}else{
					System.out.print((char)ch + " ");
					ch++;
				}
			}
			System.out.println();
		}
	}
}


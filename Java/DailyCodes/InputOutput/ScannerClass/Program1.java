/*
 * Example - 1
 * Scanner class(came after Java version 1.5) is in java.util package.
 */
import java.util.Scanner;
class Example1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name : ");
		String name = sc.next();
		/*
		 * Few methods of scanner class:
		 * next() => string
		 * nextInt() => integer
		 * nextFloat() => float
		 * nextDouble() => double
		 */

		System.out.println("Entered name is " + name);
	}
}

/*
 * Example - 2
 */

import java.util.Scanner;
class Example2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter company name : ");
		String cName = sc.next();

		System.out.println("Enter package expected : ");
		double packageExpected = sc.nextDouble();

		System.out.println("Company Name : " + cName);
		System.out.println("Package Expected : " + packageExpected);
	}
	
}

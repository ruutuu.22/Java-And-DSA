/*
 * Example - 3
 * Taking character input using bufferedreader class
 */

import java.io.*;
class Example3{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter Building name : ");
		String bName = br.readLine();					//takes string till '\n'

		System.out.println("Enter wing name : ");
		char wing = (char)br.read();				//takes only one char and keeps '\n' in the pipe
		br.skip(1);
		/*
		 * skip() of BufferedReader class is used to skip the particular number of characters from the pipe
		 */

		System.out.println("Enter flat number : ");
		int flatNo = Integer.parseInt(br.readLine());
		/*
		 * if we don't use skip() then Integer.parseInt(br.readLine()) takes the remaining '\n' from the pipe
		 * and tries to convert it into Integer but cannot convert it and them gives the exception(NumberFormatException).
		 */

		System.out.println(bName);
		System.out.println(wing);
		System.out.println(flatNo);
	}
}

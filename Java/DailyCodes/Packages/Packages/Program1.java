/*
 * Example - 1
 */

import java.util.Scanner;
import arithfun.Addition;
import arithfun.Subtraction;
import arithfun.Multiplication;

class Example1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		int x = sc.nextInt();
		int y = sc.nextInt();

		Addition obj1 = new Addition(x, y);
		System.out.println("Addition = " + obj1.add());

		Subtraction obj2 = new Subtraction(x, y);
                System.out.println("Subtraction = " + obj2.sub());

		Multiplication obj3 = new Multiplication(x, y);
                System.out.println("Multiplication = " + obj3.mul());
	}
}

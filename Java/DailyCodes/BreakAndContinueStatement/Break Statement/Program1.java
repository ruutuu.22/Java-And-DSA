/*
 * Example - 1
 * WAP to check whether numberis prime or not.
 */

class Example1{
	public static void main(String [] rutu){
		int num = 15;
		int count = 0;
		for(int i=1; i<=num; i++){
			if(num%i == 0){
				count++;
			}
			if(count > 2){
				break;
			}
		}
		if(count > 2){
			System.out.println(num + " is not prime number");
		}else{
			System.out.println(num + " is prime number");
		}
	}
}

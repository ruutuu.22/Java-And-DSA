/*
 * Example - 1
 */

class Example1{
	public static void main(String[] rutu){
		int num = 40;
		for(int i=1; i<=num; i++){
			if((i%3 == 0 && i%5 ==0) || i%4 == 0){
				continue;
			}
			System.out.println(i);
		}
	}
}

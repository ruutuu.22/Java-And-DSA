/*
 * Example - 6
 * Taking array elements as input and printing the count of even and odd elements
 */

import java.util.*;
class Example6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter array size : ");
		int size = sc.nextInt();
		int arr[] = new int [size];

		System.out.println("Enter array elements : ");
		for(int i=0; i<arr.length; i++){
			arr[i] = sc.nextInt();
		}
		
		int countEven = 0;
		int countOdd = 0;

		for(int i=0; i<arr.length; i++){
			if(arr[i]%2 == 0){
				countEven++;
			}else{
				countOdd++;
			}
		}
		System.out.println("Count of even elements in array is " + countEven);
		System.out.println("Count of odd elements in array is " + countOdd);
	}
}


/*
 * Example - 1
 */

class Example1{
	public static void main(String[] args){		//public static void main(String ... args){}
							// ...  => ellipses
		int arr[] = {10, 20, 30};
		
		for(int i=0; i<args.length; i++){
			System.out.println(args[i]);
		}
	}
}

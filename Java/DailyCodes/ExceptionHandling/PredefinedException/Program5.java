/*
 * Example - 5
 *
 * Multiple catch block can be written.
 * Child class catch block should be attended first i.e. ArithematicException Class(Child) is attended before Exception Class(Parent)
 * Object class catch block should no be used.
 */

class Example5{
	public static void main(String[] args){
		System.out.println("Start main");
		try{
			System.out.println(10/0);		
		}catch(ArithmeticException obj){
			System.out.println("ArithmeticException Class");
		}catch(Exception obj){
			System.out.println("Exception Class");
		}
		System.out.println("End main");
	}
}

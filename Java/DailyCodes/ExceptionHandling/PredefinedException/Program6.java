/*
 * Example - 6
 *
 * Multiple catch block can be written.
 * Child class catch block should be attended first i.e. ArithematicException Class(Child) is attended before Exception Class(Parent)
 * If not it gives an error that exception is already caught
 * Object class catch block should no be used.
 */

class Example6{
	public static void main(String[] args){
		System.out.println("Start main");
		try{
			System.out.println(10/0);		
		}catch(Exception obj){
			System.out.println("Exception Class");
		}catch(ArithmeticException obj){			// error: exception ArithmeticException has already been caught
			System.out.println("ArithmeticException Class");
		}
		System.out.println("End main");
	}
}

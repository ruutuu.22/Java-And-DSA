/*
 * Example - 2
 */

class Example2{
	public static void main(String[] args){
		System.out.println("Start main");
		try{
			System.out.println(10/0);	
		}catch(ArithmeticException obj){
			System.out.println(obj);		//similar to that of obj.toString();
		}
		System.out.println("End main");
	}
}

/*
 * Example - 2
 *
 * NullPointerException
 */

class Example3{
	void m1(){
		System.out.println("In m1");
	}
	void m2(){
		System.out.println("In m2");
	}

	public static void main(String[] args){
		System.out.println("Start main");
		Example3 obj = new Example3();
		obj.m1();

		obj = null;
		obj.m2();			//Exception in thread "main" java.lang.NullPointerException
		System.out.println("End main");
	}
}

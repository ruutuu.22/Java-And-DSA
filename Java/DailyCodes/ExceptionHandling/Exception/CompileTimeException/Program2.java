/*
 * Example - 2
 *
 * InterruptedException
 */

class Example2{
	public static void main(String[] args){
		for(int i=0; i<10; i++){
			System.out.println(i);
			Thread.sleep(1000);		//Thread.sleep() throws InterruptedException
		}
	}
}

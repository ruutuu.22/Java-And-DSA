/*
 * Example - 3
 */

class Example3{
	int x = 10;
	static int y = 20;

	void fun(){
		System.out.println("In Fun");
	}
	static void gun(){
		System.out.println("In gun");
	}
	public static void main(String[] args){
		Example3 obj = new Example3();
		System.out.println(obj.x);
		System.out.println(y);
		obj.fun();
		gun();
	}
}


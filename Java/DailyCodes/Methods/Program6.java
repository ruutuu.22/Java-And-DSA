/*
 * Example - 6
 */

class Example6{
	void fun(int x){
		System.out.println(x);
	}
	
	public static void main(String[] args){
		System.out.println("In main");
		Example6 obj = new Example6();
		obj.fun();
		System.out.println("End Main");
	}
}

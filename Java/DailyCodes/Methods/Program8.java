/*
 * Example - 8
 */

class Example8{
	void fun(float x){
		System.out.println("In fun");
		System.out.println(x);
	}
	
	public static void main(String[] args){
		Example8 obj = new Example8();
		obj.fun(10);
		obj.fun(10.5f);
		obj.fun('A');
	}
}


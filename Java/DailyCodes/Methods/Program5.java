/*
 * Example - 5
 */

import java.util.*;
class Example5{
	static void add(int a, int b){
		int ans = a+b;
		System.out.println("Addition = " + ans);
	}

	static void sub(int a, int b){
		int ans = a-b;
		System.out.println("Subtraction = " + ans);
	}

	static void div(int a, int b){
		int ans = a/b;
		System.out.println("Division = " + ans);
	}

	static void mul(int a, int b){
		int ans = a*b;
		System.out.println("Multiplication = " + ans);
	}

	public static void main(String[] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter int values : ");
		int a = sc.nextInt();
		int b = sc.nextInt();

		add(a, b);
		sub(a, b);
		mul(a, b);
		div(a, b);
	}
}


/*
 * Example - 10
 */

class Example10{
	int fun(int x){
		return x+10;
	}
	public static void main(String[] args){
		Example10 obj = new Example10();
		System.out.println(obj.fun(10));
	}
}

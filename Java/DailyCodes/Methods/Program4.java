/*
 * Example - 4
 */

class Example4{
	int x = 10;
	static int y = 20;
	void fun(){
		System.out.println(x);
		System.out.println(y);
	}
	public static void main(String[] args){
		Example4 obj = new Example4();
		obj.fun();
	}
}

/*
 * Example - 1
 *
 * Parent Thread Group and Child Thread Group
 */

class MyThread_Example1 extends Thread{
	MyThread_Example1(ThreadGroup tg, String str){
		super(tg, str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo_Example1{
	public static void main(String[] args){
		ThreadGroup parentTG = new ThreadGroup("Core2Web");

		MyThread_Example1 obj1 = new MyThread_Example1(parentTG, "C");
		MyThread_Example1 obj2 = new MyThread_Example1(parentTG, "Java");
		MyThread_Example1 obj3 = new MyThread_Example1(parentTG, "Python");	

		obj1.start();
		obj2.start();
		obj3.start();

		ThreadGroup childTG = new ThreadGroup(parentTG, "Incubator");

		MyThread_Example1 obj4 = new MyThread_Example1(childTG, "Flutter");
                MyThread_Example1 obj5 = new MyThread_Example1(childTG, "ReactJS");
                MyThread_Example1 obj6 = new MyThread_Example1(childTG, "SpringBoot");

                obj4.start();
                obj5.start();
                obj6.start();
	}
}

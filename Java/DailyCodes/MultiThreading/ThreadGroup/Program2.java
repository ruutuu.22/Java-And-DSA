/*
 * Example - 2
 *
 * Real Time Example
 */

class MyThread_Example2 extends Thread{
	MyThread_Example2(ThreadGroup tg, String str){
		super(tg, str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadGroupDemo_Example2{
	public static void main(String[] args){
		ThreadGroup parentTG = new ThreadGroup("ICC");

		MyThread_Example2 obj1 = new MyThread_Example2(parentTG, "BCCI");
                MyThread_Example2 obj2 = new MyThread_Example2(parentTG, "CA");
                MyThread_Example2 obj3 = new MyThread_Example2(parentTG, "WICB");

                obj1.start();
                obj2.start();
                obj3.start();

		ThreadGroup childTG1 = new ThreadGroup(parentTG, "BCCI");

		MyThread_Example2 obj4 = new MyThread_Example2(childTG1, "IPL");
                MyThread_Example2 obj5 = new MyThread_Example2(childTG1, "MPL");
                MyThread_Example2 obj6 = new MyThread_Example2(childTG1, "TNPL");

                obj4.start();
                obj5.start();
                obj6.start();

		ThreadGroup childTG2 = new ThreadGroup(parentTG, "CA");

                MyThread_Example2 obj7 = new MyThread_Example2(childTG2, "BBL");

                obj7.start();

		ThreadGroup childTG3 = new ThreadGroup(parentTG, "WICB");

                MyThread_Example2 obj8 = new MyThread_Example2(childTG3, "CPL");

                obj8.start();
	}
}

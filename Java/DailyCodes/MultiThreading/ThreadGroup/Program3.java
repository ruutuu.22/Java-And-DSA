/*
 * Example - 3
 */

class MyThread_Example3 extends Thread{
	MyThread_Example3(ThreadGroup tg, String str){
		super(tg, str);
	}
	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}
class ThreadGroupDemo_Example3{
	public static void main(String[] args) throws InterruptedException{
		ThreadGroup parentTG = new ThreadGroup("INDIA");

		MyThread_Example3 t1 = new MyThread_Example3(parentTG, "Maharashtra");
		MyThread_Example3 t2 = new MyThread_Example3(parentTG, "Goa");
		t1.start();
		t2.start();

		ThreadGroup childTG1 = new ThreadGroup(parentTG, "Pakistan");

                MyThread_Example3 t3 = new MyThread_Example3(childTG1, "Karachi");
                MyThread_Example3 t4 = new MyThread_Example3(childTG1, "Lahore");
                t3.start();
                t4.start();

		ThreadGroup childTG2 = new ThreadGroup(parentTG, "Bangladesh");

                MyThread_Example3 t5 = new MyThread_Example3(childTG2, "Karachi");
                MyThread_Example3 t6 = new MyThread_Example3(childTG2, "Mirpur");
                t5.start();
                t6.start();

		childTG1.interrupt();
		System.out.println(parentTG.activeCount());
		System.out.println(parentTG.activeGroupCount());
	}
}

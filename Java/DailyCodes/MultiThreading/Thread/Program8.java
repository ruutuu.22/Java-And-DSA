/*
 * Example - 8
 *
 * Concurrency Methods in Thread Class
 * sleep(long, int)
 */

class MyThread_Example8 extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo_Example8{
	public static void main(String[] args) throws InterruptedException{
		System.out.println(Thread.currentThread());

		MyThread_Example8 obj = new MyThread_Example8();
		obj.start();

		Thread.sleep(1000, 999999);
		/*
		 * Causes the currently executing thread to sleep (temporarily cease
		 * execution) for the specified number of milliseconds plus the specified
		 * number of nanoseconds, subject to the precision and accuracy of system
		 * timers and schedulers. The thread does not lose ownership of any
		 * monitors.
		 *
		 * @param  millis
		 * 	the length of time to sleep in milliseconds
		 *
		 * @param  nanos
		 * 	0-999999 additional nanoseconds to sleep
		 *
		 * @throws  IllegalArgumentException
		 * 	if the value of {@code millis} is negative or the value of
                 *      {@code nanos} is not in the range 0-999999	
		 *
		 * @throws  InterruptedException
		 * 	if any thread has interrupted the current thread. The
		 * 	interrupted status of the current thread is
		 * 	cleared when this exception is thrown.
		 */

		System.out.println(Thread.currentThread());
	}
}


/*
 * Example - 9
 *
 * Concurrency Methods in Thread Class
 * join()
 */

class MyThread_Example9 extends Thread{
	public void run(){
		for(int i=0; i<10; i++){
			System.out.println("In Thread - 0");
		}
	}
}

class ThreadDemo_Example9{
	public static void main(String[] args) throws InterruptedException{
		MyThread_Example9 obj = new MyThread_Example9();
		obj.start();

		obj.join();

		for(int i=0; i<10; i++){
                        System.out.println("In main");
                }
	}
}

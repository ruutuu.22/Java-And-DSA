/*
 * Example - 13
 */

class MyThread_Example13 extends Thread{
	MyThread_Example13(){
                super();
        }
	MyThread_Example13(String str){
		super(str);
	}
	public void run(){
		System.out.println(getName());
		System.out.println(Thread.currentThread());
	}
}

class ThreadDemo_Example13{
	public static void main(String[] args){
		MyThread_Example13 obj1 = new MyThread_Example13("ABC");
		obj1.start();

		MyThread_Example13 obj2 = new MyThread_Example13("ABC");
                obj2.start();

		MyThread_Example13 obj3 = new MyThread_Example13("DEF");
                obj3.start();

		MyThread_Example13 obj4 = new MyThread_Example13();
                obj4.start();
	}
}

/*
 * Example - 5
 */

class MyThread_Example5 extends Thread{
	public void run(){
		System.out.println("In run");
	}
}
class ThreadDemo_Example5{
	public static void main(String[] args){
		System.out.println("In main");
		MyThread_Example5 obj = new MyThread_Example5();

		obj.start();

		obj.start();		//Exception in thread "main" java.lang.IllegalThreadStateException
	}
}

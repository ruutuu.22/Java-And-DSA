/*
 * Example - 10
 *
 * join() creates a deadlock if both the thread join each other.
 */

class MyThread_Example10 extends Thread{
	static Thread mnName = null;
	public void run(){
		try{
			mnName.join();
		}catch(InterruptedException obj){
		}
		for(int i=0; i<10; i++){
			System.out.println("In Thread - 0");
		}
	}
}

class ThreadDemo_Example10{
	public static void main(String[] args) throws InterruptedException{

		MyThread_Example10.mnName = Thread.currentThread();
		
		MyThread_Example10 obj = new MyThread_Example10();
		obj.start();

		obj.join();

		for(int i=0; i<10; i++){
                        System.out.println("In main");
                }
	}
}

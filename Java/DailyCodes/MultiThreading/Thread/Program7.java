/*
 * Example - 7
 *
 * Concurrency Methods in Thread Class
 * sleep(long)
 */

class MyThread_Example7 extends Thread{
	public void run(){
		System.out.println(Thread.currentThread());
	}
}
class ThreadDemo_Example7{
	public static void main(String[] args) throws InterruptedException{
		System.out.println(Thread.currentThread());

		MyThread_Example7 obj = new MyThread_Example7();
		obj.start();

		Thread.sleep(1000);
		/*
		 * Causes the currently executing thread to sleep (temporarily cease
		 * execution) for the specified number of milliseconds, subject to
		 * the precision and accuracy of system timers and schedulers. The thread
		 * does not lose ownership of any monitors.
		 *
		 * @param  millis
		 * 	the length of time to sleep in milliseconds
		 *
		 * @throws  IllegalArgumentException
		 * 	if the value of {@code millis} is negative	
		 *
		 * @throws  InterruptedException
		 * 	if any thread has interrupted the current thread. The
		 * 	interrupted status of the current thread is
		 * 	cleared when this exception is thrown.
		 */

		Thread.currentThread().setName("ABCD");
		System.out.println(Thread.currentThread());
	}
}


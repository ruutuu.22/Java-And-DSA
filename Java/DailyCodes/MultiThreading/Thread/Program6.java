/*
 * Example - 6
 *
 * Priority of Threads
 */

class MyThread_Example6 extends Thread{
	public void run(){
		Thread t = Thread.currentThread();
		System.out.println(t);
		System.out.println(t.getName() + " : " + t.getPriority());
	}
}
class ThreadDemo_Example6{
	public static void main(String[] args){
		Thread t = Thread.currentThread();
		System.out.println(t);
		System.out.println(t.getName() + " : " + t.getPriority());

		MyThread_Example6 obj1 = new MyThread_Example6();
		obj1.start();
		t.setPriority(7);		//t.setPriority(11); ==> IllegalArguementException

		MyThread_Example6 obj2 = new MyThread_Example6();
		obj2.start();		
	}
}

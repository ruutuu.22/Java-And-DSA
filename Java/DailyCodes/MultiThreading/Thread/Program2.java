/*
 * Example - 2
 *
 * This program runs only on one thread,
 * 2nd thread is introduced but not used.
 */

class MyThread_Example2 extends Thread{
	public void run(){
		System.out.println("In run");
		System.out.println(Thread.currentThread().getName());
	}
	public void start(){
		System.out.println("In MyThread_Example2 start");
		run();
	}
}
class ThreadDemo_Example2{
	public static void main(String[] args){
		MyThread_Example2 obj = new MyThread_Example2();
		obj.start();
		System.out.println(Thread.currentThread().getName());
	}
}

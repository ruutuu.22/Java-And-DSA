/*
 * Example - 12
 *
 * Concurrency Methods in Thread Class
 * yeild()
 */

class MyThread_Example12 extends Thread{
	public void run(){
		System.out.println(Thread.currentThread().getName());
	}
}
class ThreadDemo_Example12{
	public static void main(String[] args){
		MyThread_Example12 obj = new MyThread_Example12();
		obj.start();
		obj.yield();
		System.out.println(Thread.currentThread().getName());
	}
}

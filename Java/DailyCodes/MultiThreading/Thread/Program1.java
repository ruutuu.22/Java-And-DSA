/*
 * Example - 1
 *
 * Creating thread using Thread class
 */

class MyThread_Example1 extends Thread{				 
	public void run(){				
		//throws cannot be used as run() of thread class thorws no exception
		//it is an overriden method therefore try catch block should be implemented
		
		System.out.println("Thread name = " + Thread.currentThread().getName());
		for(int i=0; i<10; i++){
			System.out.println("In run");
			try{
				Thread.sleep(1000);
			}catch(InterruptedException obj){
			
			}
		}
	}
}

class ThreadDemo_Example1{
	public static void main(String[] args) throws InterruptedException{
		MyThread_Example1 obj = new MyThread_Example1();		//creation of thread
		obj.start();							//starting of created thread

		System.out.println("Thread name = " + Thread.currentThread().getName());
		for(int i=0; i<10; i++){
			System.out.println("In main");
			Thread.sleep(1000);
		}
	}
}

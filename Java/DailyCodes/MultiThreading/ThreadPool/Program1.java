/*
 * Example - 1
 *
 * ExecutorService newFixedThreadPool(int);
 */

import java.util.concurrent.*;
class MyThread_Example1 implements Runnable{
	int num;
	MyThread_Example1(int num){
		this.num = num;
	}
	public void run(){
		System.out.println(Thread.currentThread() + " Start Thread : " + num);
		dailyTask();
		System.out.println(Thread.currentThread() + " End Thread : " + num);
	}
	void dailyTask(){
		try{
			Thread.sleep(8000);
		}catch(InterruptedException obj){
			obj.toString();
		}
	}
}

class ThreadPoolDemo_Example1{
	public static void main(String[] args){
		ExecutorService ser = Executors.newFixedThreadPool(3);
		for(int i=1; i<=6; i++){
			MyThread_Example1 obj = new MyThread_Example1(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}

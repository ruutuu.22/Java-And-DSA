/*
 * Example - 3
 *
 * ExecutorService newSingleThreadExecutor();
 */

import java.util.concurrent.*;
class MyThread_Example3 implements Runnable{
	int num;
	MyThread_Example3(int num){
		this.num = num;
	}
	public void run(){
		System.out.println(Thread.currentThread() + " Start Thread : " + num);
		dailyTask();
		System.out.println(Thread.currentThread() + " End Thread : " + num);
	}
	void dailyTask(){
		try{
			Thread.sleep(1000);
		}catch(InterruptedException obj){
			obj.toString();
		}
	}
}

class ThreadPoolDemo_Example3{
	public static void main(String[] args){
		ExecutorService ser = Executors.newSingleThreadExecutor();
		for(int i=1; i<=6; i++){
			MyThread_Example3 obj = new MyThread_Example3(i);
			ser.execute(obj);
		}
		ser.shutdown();
	}
}

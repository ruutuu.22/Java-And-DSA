/*
 * Example - 1
 */

class Example1{
	public static void main(String[] args){
		System.out.println("Oh Pune!");
		String str = "Veg";
		switch(str){
			case "Veg":
				{
					String str1 = "Main Course";
					switch(str1){
						case "Starters":
							System.out.println("Starter Menu");
							break;

						case "Main Course":
							System.out.println("Main Course Menu");
							break;
					}
				}
				break;

			case "Non-Veg":
				{
					String str1 = "Starters";
					switch(str1){
						case "Starters":
							System.out.println("Starter Menu");
							break;

						case "Main Course":
							System.out.println("Main Course Menu");
							break;
					}
				}
				break;
		}
	}
}


/*
 * Example - 5
 */

class Example5{
	public static void main(String[] args){
		String str = "Mon";
		switch(str){
			case "Mon":
				System.out.println("Monday");
				break;

			case "Tue":
				System.out.println("Tuesday");
				break;

			default:
				System.out.println("Sunday");
				break;
		}
	}
}

//Till java version 1.6 it gives error saying that only INTEGER works in switch.

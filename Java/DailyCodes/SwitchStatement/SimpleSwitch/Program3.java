/*
 * Example - 3
 */

class Example3{
	public static void main(String[] args){
		int ch = 65;
		switch(ch){
			case 'A':
				System.out.println("Char - A");
				break;

			case 65:					//error : duplicate case label
				System.out.println("Num - 65");
				break;

			case 'B':
				System.out.println("Char - B");
				break;

			case 66:					//error : duplicate case label
				System.out.println("Num - 66");
				break;

			default:
				System.out.println("Invalid");
				break;
		}
	}
}


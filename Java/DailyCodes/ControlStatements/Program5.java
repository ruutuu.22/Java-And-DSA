/*
 * temp > 98.6 ==> high
 * 98.0 <= temp <= 98.6 ==> Normal
 * temp < 98.0 ==> low
 */

class TemperatureMeasure{
	public static void main(String[] rutu){
		float temp = 99.00f;

		if(temp > 98.6f){
			System.out.println("High");
		}else if(temp >= 98.0f && temp <= 98.6f){
			System.out.println("Normal");
		}else{
			System.out.println("Low");
		}
	}
}

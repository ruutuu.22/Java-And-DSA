/*
 * Fizz for divisible by 3
 * Buzz for divisible by 5
 * Fizz-Buzz divisible by 3 & 5
 * Not divisible by both
 */


class FizzBuzz{
	public static void main(String[] rutu){
		int x = 10;

		if(x%3 == 0 && x%5 == 0){
			System.out.println("Fizz-Buzz");
		}else if(x%3 == 0){
			System.out.println("Fizz");
		}else if(x%5 == 0){
			System.out.println("Buzz");
		}else{
			System.out.println("Not divisible by both");
		}
	}
}
